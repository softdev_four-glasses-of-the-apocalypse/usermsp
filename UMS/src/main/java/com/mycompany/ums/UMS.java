package com.mycompany.ums;

import java.util.ArrayList;

public class UMS {

    public static void main(String[] args) {
        User Admin = new User("admin", "Administrator", "Pass@word1234", 'M', 'A');
        User user1 = new User("user1", "user 1", "Pass@word1234", 'M', 'U');
        User user2 = new User("user2", "user 2", "Pass@word1234", 'F', 'U');
        User[] userArr = new User[3];
        userArr[0] = Admin;
        userArr[1] = user1;
        userArr[2] = user2;
        System.out.println("Print for userArr");
        for (User userArr1 : userArr) {
            System.out.println(userArr1);
        }

        ArrayList<User> userList = new ArrayList<>();
        userList.add(Admin);
        System.out.println(userList.get(0) + "list Size" + userList.size());
        userList.add(user1);
        System.out.println(userList.get(1) + "list Size" + userList.size());
        userList.add(user2);
        System.out.println(userList.get(2) + "list Size" + userList.size());
    }
}
